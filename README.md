

# EyeSore Ideas Document

This document corresponds to all ideas related to EyeSore (Epimetheus Games’ submission to OGPC Season 16)

Roles

Alejandro: Music at OGPC Music Eyesore Music 
Elouan: UI Design, Level Design
Ranier: Art
Noah & Carson: General Game Design And Programming

First Game Idea (Thought of at the beginning of OGPC)

At first, you’re in a pixelated 2d platformer, you have to die to beat the level I guess, all the enemies are trying to stop you from dying.
If you beat the level: There is a boss with infinite health at the end and you will end up dying sometime or another.
If you die (e.g. falling into the void somehow or getting killed by a monster somehow) you will just move on to step 2.
You die and fall into a no longer pixelated underworld-like place where the dead corpses of the enemies that you or other players killed roam around aimlessly. 
You are a ghost, so even though the corpses will try to run toward you, they will run through you and then eventually turn around and run back at you.
Your goal is to get them to slam into the wall, which is covered in some kind of toxin that kills them (not you because that would be too hard, although maybe for some levels).
Some very special miniboss-like corpses (and other animals) will have a laser that can kill you to add some challenge to it.
The laser does a varying amount of damage depending on the boss, but it should be somewhere between 5% and 20% of your main health.
You have to work your way through the world until you encounter the mosquito guards and get past them to find that they were guarding a mastermind thing boss that you have to kill so that you can respawn and complete the platformer (that last part is not part of the game).
Other Bosses: Put other bosses here (any ideas are great, even if you think it might not be good!)
Example boss: This is just an example boss, replace it with the first idea.
The Mosquito Guards: Five mosquito guards that fly around you, and you have to wave your hands around to swat them away. If they get too close to you, you will become disoriented, and not be able to move for a few seconds
Disorientation: All controls are temporarily disabled, a swirl effect should be applied, but the pause menu should still be accessible.
The BIG Mosquito Guard: A very powerful miniboss that can shoot damaging lasers at you. It has a lot of health and it also flies around. It’s slower but can’t be swatted away, and if it hits you, you get a special type of disorientation that stalls you for longer. After which it retreats and tries to shoot damaging lasers at you.
The room is covered in slime that is toxic to you. It instantly removes 25% of your max health if you touch it and you are pushed back. The mosquitos will avoid it.
The Mastermind: Not really sure about what it should be generally but asset-wise it should be very dark with a creepy face. It also shoots damaging lasers of course.
Once you beat the final boss, you respawn and have to find the kind-of-hidden exit in the platformer to go back up to the farming simulator, where the eyeball (the player) and the character in the farming simulator are reunited happily ever after until world eater turns on and the earth disappears immediately after and the character dies in the void and out of the void comes the message https://tinyurl.com/usecautionontheinternet.
Hidden Exit: You’re put back in the room with the infinite health boss, but something is a little different: There is a secret exit somewhere in the walls that is visibly visible just so that it’s not too hard.
The Link Part: maybe not in the official game just because we don’t want to rick roll the judges. // maybe we do mwahahaha

2ND GAME IDEA

A standard platformer with some twists: The player has a shockwave, but not one that can effect the other mobs, but to help the player move around. It also has the ability to turn pixelated blocks into realistic blocks, tying into the story somehow. This idea was formed some time in december, but within a month we morphed it into the final idea.

Other Ideas:
	-Rain Animation
	-Have water. The tileset would have textured blocks, but on the surface there would be waves or something. You could use wasd or arrow keys to go up, down, left and right. You would slowly sink.
	-Obstacle courses or climbing in levels.
	-Have underwater caves and maybe have swaying algae?
	-Have portals between levels, and be able to go back and forth between levels. (they would be in the tilemap)
	-HAVE AT LEAST 20-30 LEVELS.
	-MAKE LEVELS HARD (AT LEAST SOME OF THEM)
	-Natural looking levels and caves.
	-Be able to go above ground.
	-Add nature to caves (bushes, weeds?) in tilemap.
	-In part of the game, go above ground and see an industrial-looking area with barbed wire and stuff (maybe electrical cables on the ground that electrocute you if you come near?)
 
3rd & FinalGame Story: (decided on March 2nd, after officially switching game type to puzzle platformer)

By accident, the monocle/glasses falls from the sky but the eyeball pops out to prevent Christopher from seeing the secrets of the universe. Christopher chases after the eyeball. Only the original gods (creators of the universe) were powerful enough to have banished the monocle/glasses. Its descendants are the ones that are now cruelly ruling the universe, but they cannot destroy the monocle/glasses, so they have to use  the eyeball to keep it away from humanity. Christopher got a split second to see through the glasses and he was like “this is exactly what i need to defeat our cruel rulers” and so now he's running after his eyeball (with an eyepatch on of course). The eyeball though is being commanded by the rulers of the universe, and so it can't resist yet. Then eventually when the eyeball is unable to run from christopher anymore, there is going to be a big boss fight, but you realize that the cruel rulers are using you and the wires, hop back into christopher's eye and defeat the cruel ruler gods as christopher. Using the monacle/glasses, the eyeball can manipulate the inner strings controlling the universe so that the player can manipulate the world through dying, portals, and much more.

Mr. Christopher Spendlebrock is his name

Cruel ruler gods: gods of death, use strings to control living things. When a human looks through the monocle/glasses, the wire that is linked to their death is highlighted, as the cruel ruler gods know the future and know how you will die. The cruel ruler gods can also make a part of a living thing become a separate half-living thing, which is why the eyeball exists.

Boss:

Ice cream kid (The only boss because that is tiring): You get all the way to the small town square underground, and there is a kid running an ice cream stand, christopher summons the kid down to the underground area, kid scoops ice cream from ice cream cart and throws ice cream at you, slowly turns world into sky-high sundae.

Level ideas
https://docs.google.com/spreadsheets/d/1SYal9v93swjq2dQGe7RQbGYIUmPqWoXjp5oa8QcvvSk/edit#gid=0 

Compacted 3rd Idea (decided in around January, a lot based on the second idea)

You are the left eyeball of the magician Christopher Spendlebrok, whose main goal in life is to eradicate the evil gods ruling the Universe. He’s in the field outside of his house, chopping wood for his ultimate project: The [REDACTED]. He has traveled far and wide searching for all of the materials he needs to make this, but he needs one more thing just out of his grasp: The Monocle of Control. This will help him access the inner strings the evil gods use to control the world and use it against them. It sees through the barrier that the gods have put up to prevent mere humans from seeing the inner workings of the Universe, but Christopher Spendlebrok knows better. In the field, a disturbance is created. Above a huge rift has appeared in the sky, revealing a black void. And suddenly, a single monocle falls out (presumably because someone fumbled) and falls a few feet away from Christopher. Suddenly his eyes widen, and he reaches for it and puts it on. But he only sees the magic world that the monocle contains before his left eyeball suddenly becomes animated and pops out, along with the monocle. It has been brought to life by the evil gods and its only mission is to keep the monocle away from Christopher. This eyeball is you. You will travel underground, through vast underground tunnel networks, using your newfound powers to solve perplexing puzzles and hide the monocle forever. You will manipulate the strings to teleport through walls, die to destroy blocks, and melt Ice Cream Kid’s ice cream. 

How does the moveable block work?

It’s a block that can move around freely but the player cannot move it.
It can fall into portals and fall out of the other side
It can activate buttons
Neither but also Both? (It is only indirectly a receiver through portals and a sender through buttons (and maybe spikes))
Mid-march: We have the idea that this is a clone of the player so it could possibly die on spikes?

How does the portal work?

Buttons and wires can connect to it.
When a button is connected to it, the portal is activated only when the button is pressed.
When the player dies on a spike that is connected to it, the portal is toggled (so theoretically the player could go into the portal).
A block can also go into the portal.
The portal has an entrance part and an exit part (not set by the player).
Receiver (Can be affected by wires)

How does a button work?

It’s a button, it’s like a spike, but it doesn’t kill you
Sender (Activates wires)

How does the spike work?

When you die on it you die
When you die on it it activates the wire(s) connected to it. 
Sender (Activates wires)

Ice Cream Kid

So at first the boss is using hand to hand combat and chasing you until you get to a certain point and then he takes kind of a step back and starts shooting ice cream bullets that look like you but more ice cream at you and they do not turn they are essentially like the player bullets so you will always be moving. And you use puzzle related traps to escape the boss. Eventually you find a mega ray thing that melts all of his ice cream and makes him emotionally damaged and so he walks away crying about you being uncool and oversensitive and walks away. The spell is broken. When he gets back to the surface and realizes that Christopher left him a whole bunch of money and his ice cream cart and then he really happy and it shows how Christopher is good person 


Assets (In stunning 2D Ultra Low Res Pixelated)

The player


Christopher’s Magic Wand Plant


One of Eyesore’s Buttons


Eyesore’s Spike


Old boss from our old idea 


One of our tilesets


The pillow you land on in every level 


Wire Access Terminal 



-Obstacle courses or climbing in levels.
	-Have underwater caves and maybe have swaying algae?
	-Have portals between levels, and be able to go back and forth between levels. (they would be in the tilemap)
	-HAVE AT LEAST 20-30 LEVELS.
-Rain Animation
	-Have water. The tileset would have textured blocks, but on the surface there would be waves or something. You could use wasd or arrow keys to go up, down, left and right. You would slowly sink.



Music (In stunning MP3 & WAV) (All created by our team but mostly Alejandro Belgique)


Original Soundtrack https://drive.google.com/drive/folders/19qrs65xfPFV6a2mlYJPI_q9AUICxQHVN 

Messy slideshow that only some people can view https://docs.google.com/presentation/d/1CdSvfa5OM313vIXpVDqPNO8hCCkvMtqrG6bZuVv5eE0/edit#slide=id.p 





